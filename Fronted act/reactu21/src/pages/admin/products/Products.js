import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import swal from 'sweetalert';
import Footer from '../../../components/Footer';
import Header from '../../../components/Header';
import Navbar from '../../../components/Navbar';
import Sidebar from '../../../components/Sidebar';
import APIInvoke from '../../../utils/APIInvoke';

const Products = () => {

    const [users, setUsers] = useState(
        []
    );

    const loadUsers = async () => {
        const response = await APIInvoke.invokeGET(`/products/list`);
        setUsers(response);
    }

    const deleteUser = async (e, id) => {

        e.preventDefault();

        swal({
            title: 'Delete product',
            icon: 'error',
            text: 'Are you sure you want to delete this product?',
            buttons: {
                confirm: {
                    text: 'Delete',
                    value: true,
                    visible: true,
                    className: 'btn btn-danger',
                    closeModal: true
                },
                cancel: {
                    text: 'Cancel',
                    value: false,
                    visible: true,
                    className: 'btn btn-primary',
                    closeModal: true
                }
            }
        }).then(async (value) => {

            if (value) {

                const response = await APIInvoke.invokeDELETE(`/products/${id}`)

                swal({
                    title: 'Delete product',
                    icon: 'success',
                    text: 'Product deleted successfully',
                    buttons: {
                        confirm: {
                            text: 'Close',
                            value: true,
                            visible: true,
                            className: 'btn btn-primary',
                            closeModal: true
                        }
                    }
                });

                loadUsers();
            }
        })


    }


    useEffect(() => {
        loadUsers();
    }, []);

    return (
        <div className='wrapper'>

            <Navbar></Navbar>
            <Sidebar></Sidebar>

            <div className="content-wrapper">

                <Header
                    title={"List Products"}
                    module={"Products"} >
                </Header>

                <section className="content">
                    <Link to={"#"} className="btn btn-sm btn-success">Create Product</Link>
                    <br/> <br/>
                    <table className="table table-striped projects">
                        <thead>
                            <tr>
                                <th style={{ width: '20%' }}> Name </th>
                                <th style={{ width: '30%' }}> Price </th>
                                <th> Password </th>
                                <th style={{ width: '8%' }} className="text-center"> Updated </th>
                                <th style={{ width: '20%' }}> </th>
                            </tr>
                        </thead>
                        <tbody>

                            {
                                users.map(
                                    item =>
                                        <tr key={item._id}>
                                            <td> {item.name} </td>
                                            <td> {item.price} </td>                                            
                                            <td className="project-actions text-right">
                                                <Link className="btn btn-primary btn-sm" to={`/admin/users/view/${ item._id }`} title='View'>
                                                    <i className="fas fa-eye">
                                                    </i>
                                                </Link>
                                                &nbsp;&nbsp;
                                                <Link className="btn btn-info btn-sm" to={"#"} title='Edit'>
                                                    <i className="fas fa-pencil-alt">
                                                    </i>
                                                </Link>
                                                &nbsp;&nbsp;
                                                <button className="btn btn-danger btn-sm"
                                                    onClick={(e) => { deleteUser(e, item._id) }} title='Delete'>
                                                    <i className="fas fa-trash"> </i>
                                                </button>
                                            </td>
                                        </tr>
                                )
                            }


                        </tbody>
                    </table>

                </section>
            </div>

            <Footer></Footer>

        </div>
    )
}

export default Products;