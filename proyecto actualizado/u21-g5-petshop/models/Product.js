const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: { type: String, required: true},
    brand: {type: String, required: true},
    price: { type: Number, required: true},
    stock: { type: String, required: true},

    category: [
        {
            ref: 'Category',
            type: mongoose.Types.ObjectId,
        }
    ],

    provider: [
        {
            ref: 'Provider',
            type: mongoose.Types.ObjectId,
        }
    ]

},
{
    timestamps: true,
    versionKey: false,
});


module.exports = mongoose.model('Product',productSchema)