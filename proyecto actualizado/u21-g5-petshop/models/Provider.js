const mongoose = require('mongoose');

const providerSchema = new mongoose.Schema({
    nombre: { type: String, required: true},
    telefono: { type: Number, required: true},
    direccion: { type: String, required: true},
    ciudad: { type: String, required: true}},
{
    timestamps: true,
    versionKey: false
})

module.exports = mongoose.model('Provider',providerSchema)