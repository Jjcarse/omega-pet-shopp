const app = require('./app');
const mongodbConnection = require('./db/database');

const productroutes = require('./routes/productRoutes')    

//Conexión a la base de datos

mongodbConnection();



//Inicializaicon de rutas
app.use('/',productroutes);


//Servidor
const PORT = 4000;

app.listen( PORT, ()=> {
    console.log("Server ready on port", PORT);
});