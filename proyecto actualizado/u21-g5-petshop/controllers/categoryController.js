const Category = require("../models/Category")


const listCategories = async (req, res) => {
    try {
        const categories = await Category.find();
        res.json(categories);
    } catch (error) {
        req.send(error);
    }
}
function findCategory( req, res ) {
    
    let id = req.params.id;
    let query = Category.findById(id);

    query.exec( ( err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });

}

const addCategory= async (req, res) => {
    try{
        const newCategory = new Category(req.body);
        const savedCategory = await newCategory.save();
        res.json(savedCategory);
    }catch(error){
        res.send(error);
    }
}

function updateCategory( req, res ){

    let id = req.params.id;
    let data = req.body;

    Category.findByIdAndUpdate( id, data, { new: true }, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );

}

function deleteCategory( req, res ){

    let id = req.params.id;

    Category.findByIdAndDelete( id, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );
    
}


module.exports = {
    listCategories,
    addCategory,
    findCategory,
    updateCategory,
    deleteCategory
}