
const User = require("../models/User");
const bcrypt = require("bcrypt");

const listUsers = async (req, res) => {
    try {
        const users = await User.find()//.populate('role');
        res.json(users);
    } catch (error) {
        req.send(error);
    }
}
function findUser( req, res ) {
    
    let id = req.params.id;
    let query = User.findById(id)//.populate('role');

    query.exec( ( err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });

}


const addUser = async (req, res) => {
    let myUser = new User( req.body );

    const user = await User.findOne({
        email: myUser.email
    });

    if( user != null ){
        
        res.status(403).send({ message: "User is already registered"});    
        return;
    }else{
    
        // generate salt to hash password
        const salt = await bcrypt.genSalt(10);
        myUser.password = await bcrypt.hash( myUser.password , salt);

        myUser.save( ( err, result ) => {
            if(err){
                res.status(500).send( { message: err } );
            }else{
                res.status(200).send( { message: result });
            }
        });
    }


}

function updateUser( req, res ){

    let id = req.params.id;
    let data = req.body;

    User.findByIdAndUpdate( id, data, { new: true }, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );

}

function deleteUser( req, res ){

    let id = req.params.id;

    User.findByIdAndDelete( id, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );
    
}


module.exports = {
    listUsers,
    addUser,
    findUser,
    updateUser,
    deleteUser
}