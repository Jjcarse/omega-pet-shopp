const Role = require("../models/Role");

const listRoles = async (req, res) => {
    try {
        const Roles = await Role.find();
        res.json(Roles);
    } catch (error) {
        req.send(error);
    }
}
function findRole( req, res ) {
    
    let id = req.params.id;
    let query = Role.findById(id);

    query.exec( ( err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    });

}

const addRole = async (req, res) => {
    try{
        const newRole = new Role(req.body);
        const savedRole = await newRole.save();
        res.json(savedRole);
    }catch(error){
        res.send(error);
    }
}

function updateRole( req, res ){

    let id = req.params.id;
    let data = req.body;

    Role.findByIdAndUpdate( id, data, { new: true }, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );

}

function deleteRole( req, res ){

    let id = req.params.id;

    Role.findByIdAndDelete( id, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );
    
}


module.exports = {
    listRoles,
    addRole,
    findRole,
    updateRole,
    deleteRole
}