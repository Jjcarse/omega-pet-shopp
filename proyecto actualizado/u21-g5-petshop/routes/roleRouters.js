const express = require('express');


const {listRoles, addRole,findRole, updateRole, deleteRole} = require('../controllers/roleController');

const router = express.Router();


router.get('/roles/list',listRoles);
router.get('/roles/:id',findRole);
router.post('/roles/add',addRole);

router.put('/roles/:id', updateRole);
router.delete('/roles/:id',deleteRole );

module.exports = router;