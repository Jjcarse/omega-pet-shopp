const express = require('express');
const authController = require('../controllers/authController');


const {listUsers, addUser,findUser, updateUser, deleteUser} = require('../controllers/userController');

const router = express.Router();

router.get('/users/list',listUsers);
router.get('/users/:id',findUser);
router.post('/users/add',addUser);

router.put('/users/:id', updateUser);
router.delete('/users/:id',deleteUser );

router.post('/auth/login', authController.login);
router.post('/auth/verify', authController.verifyToken, authController.verify);


module.exports = router;