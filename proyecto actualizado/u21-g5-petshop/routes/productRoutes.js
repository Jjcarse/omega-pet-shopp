const express = require('express');


const {listProducts, addProduct,findProduct, updateProduct, deleteProduct} = require('../controllers/productController');

const router = express.Router();


router.get('/products/list',listProducts);
router.get('/products/:id',findProduct);
router.post('/products/add',addProduct);

router.put('/products/:id', updateProduct);
router.delete('/products/:id',deleteProduct );

module.exports = router;